var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Factory = require('./factories/module.factory.js');

var routes = require('./routes/index');
var ping = require('./routes/ping');
// var pingId = require('./routes/pingId');
var users = require('./routes/users');

var app = express();

// connect to db
mongoose.connect('mongodb://localhost/dbmechante');
var db = mongoose.connection;
db.on('error', function callback() {
  console.log('Mongo connection error');
});
db.once('open', function callback() {
  console.log('Mongo connection established');
});
var factory = new Factory(Schema, mongoose);
factory.createSchemas();
factory.insertPeople();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/ping', ping);
app.use('/users', users);

app.get('/ping/:id', function (req, res) {
  console.log(req.params);
  res.send({ping:'hello this is server and I am got '+req.params.id});
});

app.get('/person/:name', function (req, res) {
  var resp = factory.getPerson({name: req.params.name}, res);
});

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
