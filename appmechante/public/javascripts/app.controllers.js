var mechante = angular.module('mechante', []);
mechante.controller('mechantCtrl', ['$scope', '$http', function ($scope, $http, mechantService) {
  $scope.people = [];
  $scope.lastNamed = '';
  $scope.getPerson = function (name) {
    if ($scope.lastNamed !== name) {
      $http.get('/person/' + name).then(function (result) {
        $scope.lastNamed = name;
        $scope.people = result.data;
      });
    }
  }
}]);
