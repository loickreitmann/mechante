var express = require('express');
var router = express.Router();

/* GET ping data */
router.get('/', function(req, res) {
  res.send({ping:'hello this is server and I am alive!'});
});

module.exports = router;
