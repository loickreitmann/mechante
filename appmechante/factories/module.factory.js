var Factory = function (Schema, mongoose) {
  this.Schema = Schema;
  this.mongoose = mongoose;
  this.Item = null;

  this.createSchemas = function () {
    var PersonSchema = new this.Schema({
      name: String,
      surname: String,
      age: Number
    });
    this.Person = mongoose.model('Person', PersonSchema);
  };

  this.insertPeople = function () {
    var patrick = new this.Person({
      name: 'Patrick',
      surname: 'Dempsey',
      age: 48
    });
    var joe = new this.Person({
      name: 'Joe',
      surname: 'Foster',
      age: 48
    });
    patrick.save();
    joe.save();
  };

  this.getPerson = function (query, res) {
    this.Person.find(query, function (error, output) {
      res.json(output);
    });
  };
};

module.exports = Factory;
